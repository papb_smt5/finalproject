package id.ub.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText etJudul =findViewById(R.id.etJudul);
        final EditText etDeskripsi =findViewById(R.id.etDeskripsi);
        Button bt = findViewById(R.id.btInsert);
        DAOFilm dao = new DAOFilm();
        bt.setOnClickListener(v->
        {
            //add
            Film flm = new Film(etJudul.getText().toString(),etDeskripsi.getText().toString());
            dao.add(flm).addOnSuccessListener(suc->
            {
                Toast.makeText(this,"Data Berhasil Ditamabahkan",Toast.LENGTH_SHORT).show();
            }).addOnFailureListener(fail->
            {
                Toast.makeText(this,""+fail.getMessage(),Toast.LENGTH_SHORT).show();
            });

            //test update
//            HashMap<String,Object> hashMap = new HashMap<>();
//            hashMap.put("judul",etJudul.getText().toString());
//            hashMap.put("deskripsi",etDeskripsi.getText().toString());
//            dao.update("https://papb-6479e-default-rtdb.firebaseio.com/Film/-NHx2ggpNtGb5A4qCd29",hashMap).addOnSuccessListener(suc->
//            {
//                Toast.makeText(this,"Data Berhasil Diperbarui",Toast.LENGTH_SHORT).show();
//            }).addOnFailureListener(fail->
//            {
//                Toast.makeText(this,""+fail.getMessage(),Toast.LENGTH_SHORT).show();
//            });

            //test delete
//            dao.remove("https://papb-6479e-default-rtdb.firebaseio.com/Film/-NHx2ggpNtGb5A4qCd29").addOnSuccessListener(suc->
//            {
//                Toast.makeText(this,"Data Berhasil Dihapus",Toast.LENGTH_SHORT).show();
//            }).addOnFailureListener(fail->
//            {
//                Toast.makeText(this,""+fail.getMessage(),Toast.LENGTH_SHORT).show();
//            });
        });
    }
}