package id.ub.finalproject;

public class Film {
    private String judul;
    private String deskripsi;
    public Film(){}
    public Film(String judul, String deskripsi){
        this.judul=judul;
        this.deskripsi=deskripsi;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
